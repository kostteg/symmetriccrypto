﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CodecCBC
{
	class Program
	{
		const string keyFilename = "key.txt";

		static void Main(string[] args)
		{
			if(args.Length < 2)
				PrintUsageAndExit();

			var mode = args[0];
			var data = args[1];

			var crypto = new AesManaged();

			 if(File.Exists(keyFilename))
				InitIVandKeyFromFile(crypto, keyFilename);
			else
				SaveIVandKeyToFile(crypto, keyFilename);

			if(mode == "enc")
			{
				using(var encryptor = crypto.CreateEncryptor())
				{
					var dataBytes = Encoding.GetEncoding(1251).GetBytes(args[1]);
					var result = encryptor.TransformFinalBlock(dataBytes, 0, data.Length);
					Console.WriteLine(result.ToHex());
				}
			}
			else if(mode == "dec")
			{
				using(var decryptor = crypto.CreateDecryptor())
				{
					var ecnBytes = data.ConvertFromHex();
					var result = decryptor.TransformFinalBlock(ecnBytes, 0, ecnBytes.Length);
					Console.WriteLine(Encoding.GetEncoding(1251).GetString(result));
				}
			}
			else
				PrintUsageAndExit();
		}

		private static void InitIVandKeyFromFile(SymmetricAlgorithm crypto, string keyFilename)
		{
			var lines = File.ReadAllLines(keyFilename);
			crypto.IV = lines[0].ConvertFromHex();
			crypto.Key = lines[1].ConvertFromHex();
		}

		private static void SaveIVandKeyToFile(SymmetricAlgorithm crypto, string keyFilename)
		{
			File.WriteAllLines(keyFilename, new [] {crypto.IV.ToHex(), crypto.Key.ToHex()});
		}

		private static void PrintUsageAndExit()
		{
			Console.WriteLine("Usage: Program.exe <enc|dec> <data>");
			Environment.Exit(-1);
		}
	}
}
