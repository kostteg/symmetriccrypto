﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using log4net.Config;

namespace PaddingOracleService
{
	class Program
	{
		static void Main(string[] args)
		{
			XmlConfigurator.Configure();
			try
			{
				var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "key");
				if(File.Exists(filePath))
					Key = File.ReadAllBytes(filePath);
				else
				{
					using(var aes = new AesManaged())
						Key = aes.Key;
					File.WriteAllBytes(filePath, Key);
				}

				AsyncListener.Start("http://+:31337/register/", ProcessRegister);
				AsyncListener.Start("http://+:31337/whoami/", ProcessWhoami);

				Thread.Sleep(Timeout.Infinite);
			}
			catch(Exception e)
			{
				log.Fatal("Fatal error in Main", e);
				Environment.Exit(-1);
			}
		}

		static async Task ProcessRegister(HttpListenerContext context)
		{
			var login = context.Request.QueryString["login"]?.Trim();
			var pass = context.Request.QueryString["pass"]?.Trim();
			var secret = context.Request.QueryString["secret"]?.Trim();
			if(login == null || pass == null)
			{
				context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
				context.Response.ContentEncoding = Encoding.UTF8;
				await context.Response.OutputStream.WriteAsync("'login' and 'pass' params must be specified", Encoding.UTF8);
				return;
			}

			var user = new User {Login = login, Password = pass, Secret = secret };
			var userBytes = user.ToJson();
			var encryptedUserBytes = CryptoHelper.Encrypt(userBytes, Key);

			context.Response.StatusCode = (int)HttpStatusCode.OK;
			context.Response.SetCookie(new Cookie("user", encryptedUserBytes.ToHex()));

			user.Secret = secret;
			users.TryAdd(login, user);
		}

		static async Task ProcessWhoami(HttpListenerContext context)
		{
			var cookie = context.Request.Cookies["user"];
			byte[] encryptedUserBytes;
			if(cookie == null || !cookie.Value.TryConvertFromHex(out encryptedUserBytes))
			{
				context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
				context.Response.ContentEncoding = Encoding.UTF8;
				await context.Response.OutputStream.WriteAsync("Register or login, please", Encoding.UTF8);
				return;
			}

			var userBytes = CryptoHelper.Decrypt(encryptedUserBytes, Key);

			User userFromCookie;
			try
			{
				userFromCookie = JsonHelper.ParseJson<User>(userBytes);
			}
			catch(Exception e)
			{
				context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
				context.Response.ContentEncoding = Encoding.UTF8;
				await context.Response.OutputStream.WriteAsync("Login again, please", Encoding.UTF8);
				return;
			}

			User realUser;
			if(!users.TryGetValue(userFromCookie.Login, out realUser) || realUser.Password != userFromCookie.Password)
			{
				context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
				context.Response.ContentEncoding = Encoding.UTF8;
				await context.Response.OutputStream.WriteAsync("Register again, please", Encoding.UTF8);
				return;
			}

			context.Response.StatusCode = (int)HttpStatusCode.OK;
			await context.Response.OutputStream.WriteAsync("Your secret: " + realUser.Secret, Encoding.UTF8);
		}

		static byte[] Key;

		static ConcurrentDictionary<string, User> users = new ConcurrentDictionary<string, User>();

		private static readonly ILog log = LogManager.GetLogger(typeof(Program));
	}
}
