﻿using System.Runtime.Serialization;

namespace PaddingOracleService
{
	[DataContract]
	internal class User
	{
		[DataMember(Order = 1, Name = "secret")] public string Secret;
		[DataMember(Order = 2, Name = "login")] public string Login;
		[DataMember(Order = 3, Name = "pass")] public string Password;
	}
}