using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace PaddingOracleService
{
	static class HttpListenerExtensions
	{
		public static async Task WriteAsync(this Stream stream, string str, Encoding encoding = null)
		{
			if(encoding == null)
				encoding = Encoding.GetEncoding(1251);
			var buff = encoding.GetBytes(str);
			await stream.WriteAsync(buff, 0, buff.Length);
		}
	}
}