using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace PaddingOracleService
{
	class AsyncListener
	{
		public static void Start(string prefix, Func<HttpListenerContext, Task> processAsync)
		{
			HttpListener listener = null;
			try
			{
				listener = new HttpListener();
				listener.Prefixes.Add(prefix);
				listener.Start();
			}
			catch(Exception fatalException)
			{
				log.Fatal("Fatal error in Listener.Start", fatalException);
				Environment.Exit(-1);
			}

			new Thread(() => WorkerLoop(listener, processAsync)).Start();
		}

		private static void WorkerLoop(HttpListener listener, Func<HttpListenerContext, Task> processAsync)
		{
			while(true)
			{
				try
				{
					var context = listener.GetContext();
					Task.Run(async () =>
					{
						try
						{
							await processAsync(context);
						}
						catch(Exception ee)
						{
							log.Error("Failed to process request", ee);
							context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
						}
						finally
						{
							context.Response.Close();
						}
					});
				}
				catch(Exception e)
				{
					log.Error(e);
				}
			}
		}

		private static readonly ILog log = LogManager.GetLogger(typeof(AsyncListener));
	}
}