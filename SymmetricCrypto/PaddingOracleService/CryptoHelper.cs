﻿using System;
using System.Security.Cryptography;
using log4net;

namespace PaddingOracleService
{
	static class CryptoHelper
	{
		public static byte[] Encrypt(byte[] messageBytes, byte[] key)
		{
			using(var aes = new AesManaged())
			{
				aes.IV = new byte[16];
				aes.Key = key;

				var encryptor = aes.CreateEncryptor();
				return encryptor.TransformFinalBlock(messageBytes, 0, messageBytes.Length);
			}
		}

		public static byte[] Decrypt(byte[] encryptedBytes, byte[] key)
		{
			using(var aes = new AesManaged())
			{
				aes.IV = new byte[16];
				aes.Key = key;

				var encryptor = aes.CreateDecryptor();

//				try
//				{
					return encryptor.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);
//				}
//				catch(CryptographicException e)
//				{
//					if(!e.Message.StartsWith("Padding is invalid and cannot be removed"))
//						log.Fatal(e);
//					throw;
//				}
			}
		}

		private static readonly ILog log = LogManager.GetLogger(typeof(CryptoHelper));
	}
}